public class Converter {
	//Your names go here:
	/*
	 * @Author: Cloud Chagnon
   * @Author: Nitish Pradhan 
	 *
	*/
	private double celsiusToFahrenheit(double C){
		// TODO: The third student will implement this method
		double celcius = C;
		double fahreinhiet = (1.8*celcius)+32;
		return fahreinhiet;
	}
	private double fahrenheitToCelsius(double F){
  	// TODO: The second student will implement this method
		return 5*(F-32)/9;
	}
	public static void main(String[] args) {
		Converter convert = new Converter();
		double temp1 = 180; //celsius
		double temp2 = 250; //fahrenheit
		System.out.println("Initial temperatures: Temperature 1 is "+temp1+" Celcius, and Temperature 2 is " + temp2 + " Fahrenheit.");
		temp1 = convert.celsiusToFahrenheit(temp1); //now in f
		temp2 = convert.fahrenheitToCelsius(temp2);//now in C
		System.out.println("Converted temperatures: Temperature 1 is "+temp1+" Fahrenheit, and Temperature 2 is " + temp2 + " Celsius.");
		
		// Call CelsiusToFahrenheit to convert 180 Celsius to Fahrenheit value.
		// Call FahrenheitToCelsius to convert 250 Fahrenheit to Celsius value.
	}
}
